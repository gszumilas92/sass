const path = require('path');

module.exports = {
  mode: "production",
//   entry: "./app/entry",
  output: {
    path: path.resolve(__dirname, "dist"),
    // filename: "bundle.js",
    // publicPath: "/assets/",
  },
  devServer: {
    contentBase: './dist'
  },
  module: {
    rules: [
      {
        test: /\.sass$/,
        use: [
          'style-loader',
          'css-loader',
          'sass-loader'
        ]
      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader'
        ]
      },
    ]
  }
}